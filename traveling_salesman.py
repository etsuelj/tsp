"""
Code for The Traveling Salesman Problem (W.Kinzel/G.Reents, Physics by Computer)
"""

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

L = 15 # lattice linear dimension
T_init = L/8 # intial temperature as stated in the book

mapa = np.random.random([L,L])<0.5 # randomly scatter cities (True) in an LxL lattice
N = np.sum(mapa) # number of cities
cities = np.array(zip(np.where(mapa)[0],np.where(mapa)[1])) # coordinates of the cities
mga_layo = [] # will contain distances for each iteration

def distance(cities):
    '''Calculate total distance traveled.'''
    d = 0.
    city1,city2 = cities,np.roll(cities,-1,axis=0)
    x1,y1 = city1[:,1],city1[:,0]
    x2,y2 = city2[:,1],city2[:,0]
    d =  np.sqrt((x2-x1)**2+(y2-y1)**2) # distance of adjacent cities
    return np.sum(d) # total distance 
    
def other_routes(cities):
    '''Check out other routes'''
    new_cities = np.copy(cities)
    p = np.random.randint(N)
    l = np.random.randint(N//2)
    new_cities[p:p+l+1] = cities[p:p+l+1][::-1]
    return new_cities

def iterate(cities,T):
    new_cities = other_routes(cities)    
    #acceptability condition w/ the Boltzmann factor replaced w/ the step function
    if distance(new_cities)<distance(cities):
        cities = new_cities
    
    return cities


mga_T = []
T = T_init
for i in xrange(1500):
    mga_T.append(T)
    cities = iterate(cities,T)
    mga_layo.append(distance(cities))
    if i%10==0: T = T*0.95

plt.figure()
plt.subplot(211)
plt.plot(mga_T)  
plt.ylabel('T',size='x-large')

plt.subplot(212)
plt.plot(mga_layo)
plt.ylabel('distance traveled',size='x-large')
plt.xlabel('iterations',size='x-large')
plt.show()

#plotting the route of the traveling salesman
x = np.array(list(cities[:,1]) + list(cities[-1]))
y = np.array(list(cities[:,0])  + list(cities[-1]))
plt.figure()
plt.scatter(x,y,clip_on=0,marker='.')
plt.quiver(x[:-1], y[:-1], x[1:]-x[:-1], y[1:]-y[:-1], scale_units='xy', angles='xy', scale=1,width=0.003,headwidth=10,color='blue')
plt.title('distance traveled = %.3f \n %i cities'%(distance(cities),N))
plt.axis('off')
plt.show()

